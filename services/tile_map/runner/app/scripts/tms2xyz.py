#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import sys

from pathlib import Path, PurePath

def tms2xyz(args):
    d = Path(args.tms_tiles_directory)
    count = 0
    for f in d.rglob('*.png'):
        z = int(f.parent.parent.name)
        x = int(f.parent.name)
        y = int(f.name.replace(".png",""))
        f.rename(f"{f.parent.parent.parent}/{z}/{x}/{(2**z)-y-1}.png")
        count += 1
    print(f"tm2xyz: {count} tiles processed")

def main():

    sys.argv[1:]

    parser = argparse.ArgumentParser("tms2xyz.py")
    parser.add_argument("tms_tiles_directory",help="directory with TMS tiles to transform in xyz")
    parser.set_defaults(function=tms2xyz)

    args = parser.parse_args(sys.argv[1:])
    args.function(args)


if __name__ == "__main__":
    main()
