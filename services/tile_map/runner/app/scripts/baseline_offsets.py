#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from audioop import add
import sys

from lxml import etree


def baseline_param(args):
    tree = etree.parse(args.metadata_file_path)
    ns = {"n1": "https://psd-14.sentinel2.eo.esa.int/PSD/User_Product_Level-2A.xsd"}

    if args.param == "baseline":
        baseline = tree.xpath(
        "/n1:Level-2A_User_Product"
        "/n1:General_Info"
        "/Product_Info"
        "/PROCESSING_BASELINE",
        namespaces=ns)
        if baseline:
            print(baseline[0].text)
        else:
            print("Unknown baseline")

    elif args.param == "quantification_value":
        quantification_value =  tree.xpath(
            "/n1:Level-2A_User_Product"
            "/n1:General_Info"
            "/Product_Image_Characteristics"
            "/QUANTIFICATION_VALUES_LIST"
            "/BOA_QUANTIFICATION_VALUE",
        namespaces=ns)

        if quantification_value:
            print(quantification_value[0].text)
        else:
            print("10000")

    elif args.param in ["offset_b" + str(b) for b in range(13)]:
        band = int(args.param[len("offset_b"):])
        add_offsets =  tree.xpath(
            "/n1:Level-2A_User_Product"
            "/n1:General_Info"
            "/Product_Image_Characteristics"
            "/BOA_ADD_OFFSET_VALUES_LIST"
            "/BOA_ADD_OFFSET",
        namespaces=ns)
        if add_offsets:
            print(add_offsets[band].text)
        else:
            print("0")
    else:
        print("param not found")

def main():
    parser = argparse.ArgumentParser("baseline_offsets.py")
    parser.add_argument("metadata_file_path",help="path to the metadata file")
    parser.add_argument("param",help="parameter to read")
    parser.set_defaults(function=baseline_param)

    args = parser.parse_args(sys.argv[1:])
    args.function(args)


if __name__ == "__main__":
    main()
