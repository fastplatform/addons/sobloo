#!/bin/sh

Help() {
  echo "ndvi_processing.sh"
  echo
  echo "Use this script to generate NDVI XYZ png for Sentinel2 imagery."
  echo
  echo "Syntax: ndvi_processing.sh <INPUT_DIR> <OUTPUT_DIR> <MAX_ZOOM> <NUMBER_OF_PROCESSES> <MAX_CLOUD_PROBABILITY>"
  echo
}

set -o errexit
set -o nounset

INPUT_DIR=$1
OUTPUT_DIR=$2
MAX_ZOOM=$3
NUMBER_OF_PROCESSES=$4
MAX_CLOUD_PROBABILITY=$5

if [ -z "$INPUT_DIR" ]; then
  Help
  echo "No input file supplied"
  exit 1
fi

if [ -z "$OUTPUT_DIR" ]; then
  Help
  echo "No output folder supplied"
  exit 1
fi

if [ -z "$MAX_ZOOM" ]; then
  Help
  echo "No tiling max level supplied"
  exit 1
fi

if [ -z "$NUMBER_OF_PROCESSES" ]; then
  Help
  echo "No process number supplied"
  exit 1
fi

if [ -z "$MAX_CLOUD_PROBABILITY" ]; then
  Help
  echo "No max cloud probability (0-100) supplied"
  exit 1
fi

start=$(date +%s)

echo "[NDVI] INPUT_DIR: $INPUT_DIR"
echo "[NDVI] OUTPUT_DIR: $OUTPUT_DIR"
echo "[NDVI] MAX_ZOOM: $MAX_ZOOM"
echo "[NDVI] NUMBER_OF_PROCESSES: $NUMBER_OF_PROCESSES"
echo "[NDVI] MAX_CLOUD_PROBABILITY: $MAX_CLOUD_PROBABILITY"

file_band_4=$(find $INPUT_DIR -type f -name *_B04_10m.jp2)
file_band_8=$(find $INPUT_DIR -type f -name *_B08_10m.jp2)
file_cloud_mask=$(find $INPUT_DIR -type f -name MSK_CLDPRB_20m.jp2)
file_metadata=$(find $INPUT_DIR -type f -name MTD_MSIL2A.xml)
echo "[NDVI] Found image band 4 at: $file_band_4"
echo "[NDVI] Found image band 8 at: $file_band_8"
echo "[NDVI] Found cloud mask at: $file_cloud_mask"
echo "[NDVI] Found metadata file at: $file_metadata"

echo "[NDVI: 0/8] Get offsets from metadata"

baseline=$($(dirname "$0")/baseline_offsets.py $file_metadata baseline)
echo "[NDVI] Detected baseline: $baseline"

quantification_value=$($(dirname "$0")/baseline_offsets.py $file_metadata quantification_value)
echo "[NDVI] Detected quantification value: $quantification_value"

offset_b4=$($(dirname "$0")/baseline_offsets.py $file_metadata offset_b4)
offset_b8=$($(dirname "$0")/baseline_offsets.py $file_metadata offset_b8)
echo "[NDVI] Detected offsets for bands 4 & 8: $offset_b4 $offset_b8"

echo "[NDVI: 1a/8] Convert bands to Float32"
gdal_translate -of GTiff -ot Float32 -a_nodata 0 $file_band_4 $OUTPUT_DIR/1_b4.tif
gdal_translate -of GTiff -ot Float32 -a_nodata 0 $file_band_8 $OUTPUT_DIR/1_b8.tif
gdal_translate -of GTiff -ot Float32 -a_nodata 0 $file_cloud_mask $OUTPUT_DIR/1_clouds.tif

echo "[NDVI: 1b/8] Rescale with L2A offsets"
# After this step, we get 3 bands between 0 and 10000, with the correct offset
gdal_calc.py \
  -A $OUTPUT_DIR/1_b4.tif \
  --calc="(A+($offset_b4))/($quantification_value)*10000" \
  --outfile=$OUTPUT_DIR/1_b4_offset.tif \
  --NoDataValue=$offset_b4 \
  --overwrite \
  --quiet
gdal_calc.py \
  -A $OUTPUT_DIR/1_b8.tif \
  --calc="(A+($offset_b8))/($quantification_value)*10000" \
  --outfile=$OUTPUT_DIR/1_b8_offset.tif \
  --overwrite \
  --NoDataValue=$offset_b8 \
  --quiet

echo "[NDVI: 2/8] Calculate NDVI"
# This will produce a layer with values between -1.0 and 1.0, with no alpha
gdal_calc.py \
  -A $OUTPUT_DIR/1_b8_offset.tif \
  -B $OUTPUT_DIR/1_b4_offset.tif \
  --calc="((A-B)/(A+B))" \
  --outfile=$OUTPUT_DIR/2_ndvi.tif \
  --overwrite \
  --NoDataValue=0 \
  --quiet

echo "[NDVI: 3/8] Rescale (0.0, 1.0) values to (0, 254)"
# Rescale values between 1 and 254 (we reserve value 255 for clouds)
# Note:
# - value 0 corresponds to the clamping of NODATA values
# - value 255 will be used for cloud values
# - we trash the NDVI negative values (-1.0 to 0.0), which correspond to water/snow/ice
# --> so in the end we map NDVI 0.0-1.0 to 1-254
gdal_translate \
  -of GTiff \
  -ot Byte \
  -scale 0.0 1.0 0 254 \
  -a_nodata none \
  $OUTPUT_DIR/2_ndvi.tif \
  $OUTPUT_DIR/3_ndvi_rescaled.tif

echo "[NDVI: 4/8] Remove clouds"
echo "[NDVI: 4a/8] Compute cloud mask from probabilities"
# Transform clouds probabilities to mask
# - cloud probability <= $MAX_CLOUD_PROBABILITY --> clouds_mask=0
# - cloud probability > $MAX_CLOUD_PROBABILITY --> clouds_mask=255
gdal_calc.py \
  -A $OUTPUT_DIR/1_clouds.tif \
  --calc="(A>$MAX_CLOUD_PROBABILITY)*255" \
  --NoDataValue=128 \
  --type=Byte \
  --outfile=$OUTPUT_DIR/4a_clouds_mask.tif \
  --overwrite \
  --quiet

echo "[NDVI: 4b/8] Simplify cloud mask"
# Remove polygons that are too small (less than 12 pixels)
gdal_sieve.py \
  -q \
  -st 12 \
  -4 \
  $OUTPUT_DIR/4a_clouds_mask.tif \
  -nomask \
  -of GTiff \
  $OUTPUT_DIR/4b_clouds_mask_simplified.tif

echo "[NDVI: 4c/8] Upsample cloud mask"
# Upsample the clouds alpha layer to match the original NDVI layer
# - the NDVI layer is at 10m resolution, but the clouds are at 20m resolution
# - we use the "max" resampling, to be sure that a masked region does not get unmasked by the upsampling
gdalwarp \
  -q \
  -tr 10 10 \
  -r max \
  -overwrite \
  -multi \
  $OUTPUT_DIR/4b_clouds_mask_simplified.tif \
  $OUTPUT_DIR/4c_clouds_mask_upsampled.tif

echo "[NDVI: 4d/8] Set NDVI value to 255 when clouds"
# Set NDVI=255 on clouds
gdal_calc.py \
  -A $OUTPUT_DIR/4c_clouds_mask_upsampled.tif \
  -B $OUTPUT_DIR/3_ndvi_rescaled.tif \
  --calc="((A!=255)*B)+((A==255)*255)" \
  --NoDataValue=0 \
  --type=Byte \
  --outfile=$OUTPUT_DIR/4d_ndvi_rescaled_with_clouds.tif \
  --overwrite \
  --quiet

echo "[NDVI: 5/8] Reproject to EPSG:3857 before tiling"
gdalwarp \
  -t_srs EPSG:3857 \
  -srcnodata 0 \
  -dstalpha \
  -overwrite \
  $OUTPUT_DIR/4d_ndvi_rescaled_with_clouds.tif \
  $OUTPUT_DIR/5_ndvi_warped.tif

echo "[NDVI: 6/8] Generate TMS tiles"
# cf. --xyz --tilesize=256 for gdal >= 3.1
gdal2tiles.py \
  --profile=mercator \
  --resampling=near \
  --zoom=2-$MAX_ZOOM \
  --resume \
  --exclude \
  --processes=$NUMBER_OF_PROCESSES \
  --srcnodata=0 \
  $OUTPUT_DIR/5_ndvi_warped.tif \
  $OUTPUT_DIR

echo "[NDVI: 7/8] Convert TMS to XYZ"
# We cannot use the --xyz option of gdal2tiles, because it is not currently available
# in GDAL 2.X.X and we cannot upgrade GDAL to 3.1.X for the moment
# So we have a script that renames the tiles from the TMS scheme to the XYZ scheme
$(dirname "$0")/tms2xyz.py $OUTPUT_DIR

echo "[NDVI: 8/8] Cleanup"
find $OUTPUT_DIR -type f -not -name '*.png' -delete

end=$(date +%s)
echo "[NDVI] Done in $((end - start)) seconds"
