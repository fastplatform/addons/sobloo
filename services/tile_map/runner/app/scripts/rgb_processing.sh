#!/bin/sh

set -o errexit
set -o nounset

Help()
{
   echo "rgb_processing.sh"
   echo
   echo "Use this script to generate RGB XYZ png for Sentinel2 imagery."
   echo
   echo "Syntax: rgb_processing.sh <INPUT_DIR> <OUTPUT_DIR> <MAX_ZOOM> <NUMBER_OF_PROCESSES>"
   echo
}

INPUT_DIR=$1
OUTPUT_DIR=$2
MAX_ZOOM=$3
NUMBER_OF_PROCESSES=$4

if [ -z "$INPUT_DIR" ]; then
  Help
  echo "No input file supplied"
  exit 1
fi

if [ -z "$OUTPUT_DIR" ]; then
  Help
  echo "No output folder supplied"
  exit 1
fi

if [ -z "$MAX_ZOOM" ]; then
  Help
  echo "No tiling max level supplied"
  exit 1
fi

if [ -z "$NUMBER_OF_PROCESSES" ]; then
  Help
  echo "No process number supplied"
  exit 1
fi

start=$(date +%s)

INPUT_DIR=$1
OUTPUT_DIR=$2
MAX_ZOOM=$3
NUMBER_OF_PROCESSES=$4

echo "[RGB] INPUT_DIR: $INPUT_DIR"
echo "[RGB] OUTPUT_DIR: $OUTPUT_DIR"
echo "[RGB] MAX_ZOOM: $MAX_ZOOM"
echo "[RGB] NUMBER_OF_PROCESSES: $NUMBER_OF_PROCESSES"

# Get file for each bands
file_band_2=$(find $INPUT_DIR -type f -name *_B02_10m.jp2)
file_band_3=$(find $INPUT_DIR -type f -name *_B03_10m.jp2)
file_band_4=$(find $INPUT_DIR -type f -name *_B04_10m.jp2)
file_cloud_mask=$(find $INPUT_DIR -type f -name MSK_CLOUDS_B00.gml)
file_metadata=$(find $INPUT_DIR -type f -name MTD_MSIL2A.xml)
echo "[RGB] Found image band 2 at: $file_band_2"
echo "[RGB] Found image band 3 at: $file_band_3"
echo "[RGB] Found image band 4 at: $file_band_4"
echo "[RGB] Found cloud mask at: $file_cloud_mask"
echo "[RGB] Found metadata file at: $file_metadata"

echo "[RGB: 0/7] Get offsets from metadata"

baseline=$($(dirname "$0")/baseline_offsets.py $file_metadata baseline)
echo "[RGB] Detected baseline: $baseline"

quantification_value=$($(dirname "$0")/baseline_offsets.py $file_metadata quantification_value)
echo "[RGB] Detected quantification value: $quantification_value"

offset_b2=$($(dirname "$0")/baseline_offsets.py $file_metadata offset_b2)
offset_b3=$($(dirname "$0")/baseline_offsets.py $file_metadata offset_b3)
offset_b4=$($(dirname "$0")/baseline_offsets.py $file_metadata offset_b4)
echo "[RGB] Detected offsets for bands 2, 3 & 4: $offset_b2 $offset_b3 $offset_b4"

echo "[RGB: 1/7] Convert bands to Float32"
gdal_translate -of GTiff -ot Float32 -a_nodata 0 $file_band_4 $OUTPUT_DIR/1_b4.tif
gdal_translate -of GTiff -ot Float32 -a_nodata 0 $file_band_3 $OUTPUT_DIR/1_b3.tif
gdal_translate -of GTiff -ot Float32 -a_nodata 0 $file_band_2 $OUTPUT_DIR/1_b2.tif

echo "[RGB: 2/7] Rescale with L2A offsets"
# After this step, we get 3 bands between 0 and 10000, with the correct offset
gdal_calc.py \
  -A $OUTPUT_DIR/1_b4.tif \
  --calc="(A+($offset_b4))/($quantification_value)*10000" \
  --outfile=$OUTPUT_DIR/1_b4_offset.tif \
  --overwrite \
  --NoDataValue=$offset_b4 \
  --quiet
gdal_calc.py \
  -A $OUTPUT_DIR/1_b3.tif \
  --calc="(A+($offset_b3))/($quantification_value)*10000" \
  --outfile=$OUTPUT_DIR/1_b3_offset.tif \
  --overwrite \
  --NoDataValue=$offset_b3 \
  --quiet
gdal_calc.py \
  -A $OUTPUT_DIR/1_b2.tif \
  --calc="(A+($offset_b2))/($quantification_value)*10000" \
  --outfile=$OUTPUT_DIR/1_b2_offset.tif \
  --overwrite \
  --NoDataValue=$offset_b2 \
  --quiet

# Build TMS
echo "[RGB: 3/7] Merge bands 2 3 4"
gdalbuildvrt \
  -separate \
  $OUTPUT_DIR/1_rgb_stack.vrt \
  $OUTPUT_DIR/1_b4_offset.tif \
  $OUTPUT_DIR/1_b3_offset.tif \
  $OUTPUT_DIR/1_b2_offset.tif

echo "[RGB: 4/7] Rescale"
# For coloration purpose, rescale to roughly 0-2000 (out of 0-10000)
gdal_translate \
  -ot Byte \
  -scale 0 2000 0 255 \
  $OUTPUT_DIR/1_rgb_stack.vrt \
  $OUTPUT_DIR/2a_rgb_rescaled.tif

gdal_translate \
  -a_nodata 0 \
  $OUTPUT_DIR/2a_rgb_rescaled.tif \
  $OUTPUT_DIR/2b_rgb_rescaled_nodata.tif

gdal_translate \
  -of VRT \
  -ot byte \
  -scale \
  $OUTPUT_DIR/2b_rgb_rescaled_nodata.tif \
  $OUTPUT_DIR/2c_rgb.vrt

echo "[RGB: 5/7] Generate TMS tiles"
# cf. --xyz --tilesize=256 for gdal >= 3.1
gdal2tiles.py \
  --profile=mercator \
  --resampling=cubic \
  --zoom=2-$MAX_ZOOM \
  --resume \
  --exclude \
  --processes=$NUMBER_OF_PROCESSES \
  $OUTPUT_DIR/2c_rgb.vrt \
  $OUTPUT_DIR

echo "[RGB: 6/7] Convert TMS to XYZ"
$(dirname "$0")/tms2xyz.py $OUTPUT_DIR

echo "[RGB: 7/7] Cleanup"
find $OUTPUT_DIR -type f -not -name '*.png' -delete

end=$(date +%s)
echo "[RGB] Done in $((end-start)) seconds"