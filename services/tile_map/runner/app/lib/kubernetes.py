import logging
import hashlib

from datetime import datetime
from kubernetes_asyncio import client, config as kubernetes_config
from kubernetes_asyncio.client.api_client import ApiClient
from kubernetes_asyncio.client.rest import ApiException

from app.settings import Settings, config


class KubernetesClient:

    api: ApiClient
    v1: client.BatchV1Api

    def __init__(self):
        self.api = None
        self.v1 = None

    async def connect_client(self):
        """Create the Kubernetes client and connect it"""
        if config.KUBERNETES_INCLUSTER:
            kubernetes_config.load_incluster_config()
        else:
            await kubernetes_config.load_kube_config()

        self.api = ApiClient()
        self.v1 = client.BatchV1Api(self.api)

    async def close_client(self):
        """Close the client"""
        await self.api.close()

    async def submit_satellite_image_processing_job(self, satellite_image_id: str):
        """Submit a job to the Kubernetes cluster that will download and process the RGB and NDVI
        for the provided satellite image identifier.
        This function must be indepotent. For a given satellite_image_id, the exact same YAML
        manifest must be submited.
        """

        job_config = {
            "apiVersion": "batch/v1",
            "kind": "Job",
            "metadata": {
                "labels": {
                    "tile-map-runner-process-image-id": f"{satellite_image_id}",
                },
                "name": f"tile-map-runner-process-{hashlib.md5(satellite_image_id.encode()).hexdigest()}",
            },
            "spec": {
                "backoffLimit": 1,
                "template": {
                    "spec": {
                        "containers": [
                            {
                                "name": "tiles-processor",
                                "command": [
                                    "/bin/sh",
                                    "-c",
                                    f"""
                                    until curl -fsI http://localhost:15020/healthz/ready; do echo \"Waiting for sidecar proxy ...\"; python -c 'import time;time.sleep(1)'; done;
                                    python ../../*.binary process {satellite_image_id}
                                    exit_code=$(echo $?);
                                    curl -fsI -X POST http://localhost:15020/quitquitquit && exit $exit_code""",
                                ],
                                "image": f"{config.KUBERNETES_JOB_IMAGE}",
                                "imagePullPolicy": "Always",
                                "env":[
                                    {
                                        "name": "CATALOG_ADMIN_SECRET_KEY",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "catalog-admin-key",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    },
                                    {
                                        "name": "CATALOG_TIMEOUT",
                                        "value": f"{config.CATALOG_TIMEOUT}",
                                    },
                                    {
                                        "name": "CATALOG_URL",
                                        "value": f"{config.CATALOG_URL}",
                                    },
                                    {
                                        "name": "LOG_LEVEL",
                                        "value": f"{config.LOG_LEVEL}",
                                    },
                                    {
                                        "name": "MAX_ZOOM",
                                        "value": f"{config.MAX_ZOOM}",
                                    },
                                    {
                                        "name": "NUMBER_OF_PROCESSES",
                                        "value": f"{config.NUMBER_OF_PROCESSES}",
                                    },
                                    {
                                        "name": "MAX_CLOUD_PROBABILITY",
                                        "value": f"{config.MAX_CLOUD_PROBABILITY}",
                                    },
                                    {
                                        "name": "WORKING_DIR",
                                        "value": "/tmp/job/working_dir",
                                    },
                                    {
                                        "name": "REMOTE_DEBUG",
                                        "value": f"{config.REMOTE_DEBUG}",
                                    },
                                    {
                                        "name": "S3_ACCESS_KEY_ID",
                                        "value": f"{config.S3_ACCESS_KEY_ID}",
                                    },
                                    {
                                        "name": "S3_LOCATION",
                                        "value": f"{config.S3_LOCATION}",
                                    },
                                    {
                                        "name": "S3_BUCKET_NAME",
                                        "value": f"{config.S3_BUCKET_NAME}",
                                    },
                                    {
                                        "name": "S3_URL",
                                        "value": f"{config.S3_URL}",
                                    },
                                    {
                                        "name": "S3_RETRIES",
                                        "value": f"{config.S3_RETRIES}",
                                    },
                                    {
                                        "name": "S3_SECRET_KEY",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "s3-secret-key",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    },
                                    {
                                        "name": "S3_UPLOAD_CONCURRENCY",
                                        "value": f"{config.S3_UPLOAD_CONCURRENCY}",
                                    },
                                    {
                                        "name": "EODAG_PROVIDERS",
                                        "value": f"{config.EODAG_PROVIDERS}",
                                    },
                                    {
                                        "name": "EODAG__SCIHUB__API__CREDENTIALS__USERNAME",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "scihub-username",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    },
                                    {
                                        "name": "EODAG__SCIHUB__API__CREDENTIALS__PASSWORD",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "scihub-password",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    },
                                    {
                                        "name": "EODAG__SOBLOO__AUTH__CREDENTIALS__APIKEY",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "sobloo-apikey",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    },
                                    {
                                        "name": "EODAG__CREODIAS__AUTH__CREDENTIALS__USERNAME",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "creodias-username",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    },
                                    {
                                        "name": "EODAG__CREODIAS__AUTH__CREDENTIALS__PASSWORD",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "creodias-password",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    },
                                    {
                                        "name": "EODAG__ONDA__AUTH__CREDENTIALS__USERNAME",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "onda-username",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    },
                                    {
                                        "name": "EODAG__ONDA__AUTH__CREDENTIALS__PASSWORD",
                                        "valueFrom": {
                                            "secretKeyRef": {
                                                "key": "onda-password",
                                                "name": "tile-map-runner-secret",
                                            },
                                        },
                                    }
                                ],
                                "resources": {
                                    "limits": {
                                        "cpu": f"{config.KUBERNETES_JOB_RESOURCES_CPU}",
                                        "memory": f"{config.KUBERNETES_JOB_RESOURCES_MEMORY}",
                                    },
                                },
                                "volumeMounts": [
                                    {
                                        "name": "working-directory",
                                        "mountPath": "/tmp/job/working_dir",
                                    }
                                ],
                            }
                        ],
                        "volumes": [
                            {
                                "name": "working-directory",
                                "emptyDir": {},
                            }
                        ],
                        "restartPolicy": "Never",
                    }
                },
            },
        }

        api_response = None

        try:
            api_response = await self.v1.create_namespaced_job(
                config.KUBERNETES_JOB_NAMESPACE, body=job_config
            )
        except ApiException as e:
            logging.error(
                "Exception when calling BatchV1Api->create_namespaced_job: %s", e
            )
            api_response = e

        return api_response

    async def clean_successful_satellite_image_processing_jobs(self):
        """Clean successful Kubernetes processing jobs
        """

        try:
            sucessful_jobs_to_delete = await self.v1.list_namespaced_job(
                namespace=config.KUBERNETES_JOB_NAMESPACE,
                label_selector="tile-map-runner-process-image-id",
                field_selector="status.successful=1",
            )

            await self.v1.delete_collection_namespaced_job(
                namespace=config.KUBERNETES_JOB_NAMESPACE,
                label_selector="tile-map-runner-process-image-id",
                field_selector="status.successful=1",
                propagation_policy="Background",
            )

        except ApiException as e:
            logging.error(
                "Exception when calling BatchV1Api->create_namespaced_job: %s", e
            )
            api_response = e

        return sucessful_jobs_to_delete.items

    async def clean_failed_satellite_image_processing_jobs(self):
        """Clean successful Kubernetes processing jobs
        """

        try:
            jobs = await self.get_image_processing_jobs()

            failed_job_to_delete = []
            for j in jobs.items:
                if not j.status.conditions:
                    continue
                if not j.status.conditions[0].type.lower() == "failed":
                    continue
                failure_time = j.status.conditions[0].last_transition_time
                delta = datetime.now(failure_time.tzinfo) - failure_time
                if delta.total_seconds() > config.KUBERNETES_JOB_RETRY_INTERVAL:
                    failed_job_to_delete.append(j)

            await self.v1.delete_collection_namespaced_job(
                namespace=config.KUBERNETES_JOB_NAMESPACE,
                label_selector="tile-map-runner-process-image-id in ({})".format(
                    ",".join(
                        [j.metadata.labels["tile-map-runner-process-image-id"] for j in failed_job_to_delete]
                    )
                ),
                propagation_policy="Background",
            )

        except ApiException as e:
            logging.error(
                "Exception when calling BatchV1Api->create_namespaced_job: %s", e
            )
            api_response = e

        return failed_job_to_delete

    async def get_image_processing_jobs(self):
        """Get Kubernetes image processing jobs
        """

        try:

            jobs = await self.v1.list_namespaced_job(
                namespace=config.KUBERNETES_JOB_NAMESPACE,
                label_selector=f"tile-map-runner-process-image-id",
            )

        except ApiException as e:
            logging.error(
                "Exception when calling BatchV1Api->create_namespaced_job: %s", e
            )
            api_response = e

        return jobs


kubernetes_client = KubernetesClient()
