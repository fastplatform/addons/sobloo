import os
import logging
import glob
from multiprocessing.pool import ThreadPool

import boto3
from botocore.config import Config
from botocore.exceptions import ClientError

from app.settings import config

logger = logging.getLogger(__name__)


class S3Client:
    def __init__(self):
        session = boto3.Session(
            aws_access_key_id=config.S3_ACCESS_KEY_ID,
            aws_secret_access_key=config.S3_SECRET_KEY,
        )
        s3 = session.resource(
            "s3",
            endpoint_url=config.S3_URL,
            config=Config(
                retries={"max_attempts": config.S3_RETRIES},
                max_pool_connections=config.S3_UPLOAD_CONCURRENCY,
            ),
        )
        self.bucket = s3.Bucket(config.S3_BUCKET_NAME)

    def upload_satellite_image_processing(
        self, satellite_image_id: str, process_type: str
    ):
        """Upload to S3 all the tiles (RGB and NDVI) for a satellite image

        Expects to find the tiles in the following directories:
            - WORKING_DIR/satellite_image_id/rgb/**/*.png
            - WORKING_DIR/satellite_image_id/ndvi/**/*.png

        Args:
            satellite_image_id (str): ID of the satellite image
            process_type (str): Type of processing (rgb or ndvi)
        """
        files_paths = glob.glob(
            str(config.WORKING_DIR / satellite_image_id / process_type / "**/*.png"),
            recursive=True,
        )
        files_paths = sorted(files_paths)

        logging.info(
            "Found %s %s files to upload for satellite image %s",
            len(files_paths),
            process_type,
            satellite_image_id,
        )

        # File keys: <satellite_image_id/<process>/z/x/y.png
        files_s3_keys = [
            os.path.relpath(file_path, config.WORKING_DIR) for file_path in files_paths
        ]
        if config.S3_LOCATION:
            files_s3_keys = [f"{config.S3_LOCATION}/{key}" for key in files_s3_keys]

        def upload(file_path, s3_key):
            """Upload a file to S3 under the provided key

            Args:
                file_path (str): Local file path
                s3_key (str): S3 object key

            Returns:
                bool: True if success, False otherwise
            """
            try:
                self.bucket.upload_file(
                    file_path,
                    s3_key,
                    ExtraArgs={"ContentType": "image/png"},
                )
            except ClientError as e:
                logging.error(e)
                return False
            return True

        # Start a thread pool and map the files to upload
        with ThreadPool(config.S3_UPLOAD_CONCURRENCY) as pool:
            results = pool.starmap(upload, zip(files_paths, files_s3_keys))

        logger.info(
            "Uploaded %s/%s %s tiles for satellite image %s",
            sum(results),
            len(results),
            process_type,
            satellite_image_id,
        )

        if not all(results):
            raise Exception(
                f"Failed to upload {len(results) - sum(results)} {process_type} tiles for satellite image {satellite_image_id}"
            )

    def delete_satellite_image_processings(self, satellite_image_id: str):
        """Delete from S3 the tiles (RGB and NDVI) for a satellite image

        Expects to find the tiles in the following directories:
            - S3_BUCKET_NAME/satellite_image_id/rgb/**/*.png
            - S3_BUCKET_NAME/satellite_image_id/ndvi/**/*.png

        Args:
            satellite_image_id (str): ID of the satellite image
        """
        prefix = f"{satellite_image_id}/"
        if config.S3_LOCATION:
            prefix = f"{config.S3_LOCATION}/{prefix}"

        self.bucket.objects.filter(Prefix=prefix).delete()


s3_client = S3Client()
