import os
import logging
import zipfile
import glob
from typing import List
from datetime import date, datetime, timedelta
import dateutil.parser
from pathlib import Path

from pydantic import BaseModel
from shapely.geometry import MultiPolygon

from app.settings import config

# Logging
logger = logging.getLogger(__name__)


class CatalogHit(BaseModel):
    id: str
    name: str
    geometry: dict
    cloud_cover: float
    acquisition_date: datetime
    mission: str
    mission_code: str
    sources: List[dict]


class EODAGClient:
    """A wrapper client for the eodag library.

    Manages multiple providers, merging catalogue query results and downloading
    products with fallback between providers
    """

    PROVIDERS = config.EODAG_PROVIDERS.split(",")

    def __init__(self):
        import eodag
        eodag.utils.logging.setup_logging(2)

        self.dag = eodag.EODataAccessGateway()

    def search(self) -> List[CatalogHit]:
        """Search for products in the DIAS/SciHub catalogues for our
        given settings
        - MAX_CLOUD_COVER: The maximum cloud cover allowed for a product to be returned
        - AREA_OF_INTEREST: The area of interest to search for products
        - RETENTION_PERIOD: The period of time after which a product is considered too old and should not be included the results

        Returns:
            List[CatalogHit]: A list of products that match our search settings
        """

        all_hits = []

        # Search in each of the providers, in parallel
        for provider in self.PROVIDERS:
            all_hits.extend(self._search_provider(provider))
        
        # Aggregate the hits (same product id but from different providers)
        all_hits_dict = {}
        for hit in all_hits:
            if hit.id not in all_hits_dict:
                all_hits_dict[hit.id] = hit
            else:
                # If a product is found in multiple providers, we merge the sources
                # to be able to, later, download the product from any of the initial
                # providers (for redundancy)
                all_hits_dict[hit.id].sources += hit.sources
        all_hits = list(all_hits_dict.values())

        return all_hits

    def _search_provider(self, provider: str) -> List[CatalogHit]:

        start = date.today() - timedelta(days=config.SYNCHRONIZATION_PERIOD)
        end = date.today() + timedelta(days=1)

        try:
            logger.info("Searching in %s catalog...", provider)
            self.dag.set_preferred_provider(provider)
            search_results = self.dag.search_all(
                productType="S2_MSI_L2A",
                start=start.isoformat(),
                end=end.isoformat(),
                geom=config.AREA_OF_INTEREST,
            )

            logger.info("Found %s products from %s", len(search_results), provider)

            # We filter clouded results by hand as the "crunch" property filter
            # from the eodag library does not work properly.
            filtered_results = [
                product
                for product in search_results
                if float(product.properties["cloudCover"]) <= config.MAX_CLOUD_COVER
            ]

            logger.info(
                "Filtered %s products with cloud cover <= %s%% from %s",
                len(filtered_results),
                config.MAX_CLOUD_COVER,
                provider,
            )

            # It can happen that the search returns duplicate products, remove them
            deduplicated_results = {}
            for product in filtered_results:
                deduplicated_results[product.properties["id"]] = product
            deduplicated_results = list(deduplicated_results.values())

            logger.info(
                "Deduplicated %s products from %s", len(filtered_results), provider
            )

            # Convert the EOProducts to CatalogHits
            hits = [self._parse_product(product) for product in deduplicated_results]

            return hits

        except Exception as ex:
            logger.exception("Failed search in %s catalog...", provider)
            return []

    def _parse_product(self, product) -> CatalogHit:
        """Parse an EOProduct into a CatalogHit

        Args:
            product (EOProduct): The product to parse

        Returns:
            CatalogHit: The parsed hit
        """

        if isinstance(product.geometry, MultiPolygon):
            product.geometry = product.geometry.geoms[0]

        geometry = product.geometry.__geo_interface__
        geometry["crs"] = {
            "type": "name",
            "properties": {"name": "urn:ogc:def:crs:EPSG::4326"},
        }

        return CatalogHit(
            id=product.properties["id"],
            name=product.properties["id"],
            geometry=geometry,
            cloud_cover=product.properties["cloudCover"],
            acquisition_date=dateutil.parser.isoparse(
                product.properties["startTimeFromAscendingNode"]
            ),
            mission="Sentinel-2",
            mission_code=product.properties["id"][:3],
            sources=[product.as_dict()],
        )

    def download(self, satellite_image_id: str, sources: dict) -> bool:
        """Download product from the provider

        Args:
            satellite_image_id (str): The identifier of the product
            sources (list[dict]): The source(s) product information as returned by the
                                  catalogue search

        Returns:
            boolean: True if download is a success, False if any error occurs
        """

        # Make sure our download directory exists
        download_dir = config.WORKING_DIR / satellite_image_id / "download"
        os.makedirs(download_dir, exist_ok=True)

        # Download the product using any of the available sources
        has_downloaded = False

        for source in sources:
            try:
                self._download_from_source(source, download_dir)
                has_downloaded = True
                break
            except Exception as ex:
                logger.exception(
                    "Failed to download product %s from %s",
                    source["id"],
                    source["properties"]["eodag_provider"],
                )
                # It failed with this source, try the next one
                pass

        return has_downloaded

    def _download_from_source(self, source: dict, download_dir: Path) -> None:
        # Rebuild the product from the source information
        import eodag
        product = eodag.api.product.EOProduct.from_geojson(source)

        if product.provider not in self.PROVIDERS:
            raise Exception(
                "Product %s is from provider %s, which is not in the providers list"
            )

        # Download
        logger.info(
            "Downloading product %s from %s",
            source["id"],
            source["properties"]["eodag_provider"],
        )
        file_path = self.dag.download(
            product,
            None,
            wait=2,
            timeout=30,
            outputs_prefix=os.path.abspath(download_dir),
            extract=False,
        )

        # Unzip downloaded product
        # (will overwrite files that already exist)
        zip_file = zipfile.ZipFile(file_path)
        zip_file.extractall(download_dir)

        # Check that what we have downloaded is fine
        logger.info(
            "Checking product %s from %s",
            source["id"],
            source["properties"]["eodag_provider"],
        )
        self._check_satellite_image_download_integrity(download_dir)

    def _check_satellite_image_download_integrity(self, download_dir: Path) -> None:
        """Check that what we have downloaded is ok

        Args:
            satellite_image_id (str): [description]

        Raises:
            FileNotFoundError: If any of the required files is not present
        """

        suffixes = [
            "_B02_10m.jp2",
            "_B03_10m.jp2",
            "_B04_10m.jp2",
            "_B08_10m.jp2",
            "MSK_CLDPRB_20m.jp2",
        ]

        for suffix in suffixes:
            file_paths = glob.glob(
                str(download_dir / ("**/*" + suffix)),
                recursive=True,
            )
            if not file_paths:
                raise FileNotFoundError(
                    f"File with suffix {suffix} was not found in {download_dir}"
                )


eodag_client = EODAGClient()
