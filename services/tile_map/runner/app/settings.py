import os
from pathlib import Path, PurePath

from pydantic import BaseSettings


class Settings(BaseSettings):

    APP_DIR: Path = Path(__file__).parent

    # Logging
    # -------
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info"))

    # Remote debug
    # ------------
    REMOTE_DEBUG: bool = False

    # Our catalog (GraphQL)
    # ---------------------
    CATALOG_ADMIN_SECRET_KEY: str = ""
    CATALOG_TIMEOUT: int = 60 * 5  # 5 minutes
    CATALOG_URL: str = "http://localhost.fastplatform.eu:8433/v1/graphql"
    CATALOG_WRITE_CHUNK_SIZE: int = 500
    CATALOG_DELETE_CHUNK_SIZE: int = 500

    # Our object storage (S3)
    # -----------------------
    S3_ACCESS_KEY_ID: str = ""
    S3_BUCKET_NAME: str = "fastplatform-test-tiles"
    S3_LOCATION: str = ""
    S3_RETRIES: int = 10
    S3_SECRET_KEY: str = ""
    S3_UPLOAD_CONCURRENCY: int = 50
    S3_URL: str = "https://oss.eu-west-0.prod-cloud-ocb.orange-business.com"

    # Sync
    # ----
    # WKT of the area of interest to watch
    AREA_OF_INTEREST: str = "POLYGON EMPTY"
    # Max cloud cover percentage of images that will be processed
    MAX_CLOUD_COVER: float = 30.0
    # Number of days the images/tiles are to be kept in the database/storage
    RETENTION_PERIOD: int = 365
    # Number of days of images that should be requested from the DIAS catalogue
    # and compared to our own
    SYNCHRONIZATION_PERIOD: int = 30
    # Providers to activate in EODAG for searching and downloading images
    EODAG_PROVIDERS: str = "creodias,onda,scihub"
    # SciHub credentials
    EODAG__SCIHUB__API__CREDENTIALS__USERNAME: str = ""
    EODAG__SCIHUB__API__CREDENTIALS__PASSWORD: str = ""
    # CREODIAS credentials
    EODAG__CREODIAS__AUTH__CREDENTIALS__USERNAME: str = ""
    EODAG__CREODIAS__AUTH__CREDENTIALS__PASSWORD: str = ""
    # ONDA credentials
    EODAG__ONDA__AUTH__CREDENTIALS__USERNAME: str = ""
    EODAG__ONDA__AUTH__CREDENTIALS__PASSWORD: str = ""
    # Sobloo credentials
    EODAG__SOBLOO__AUTH__CREDENTIALS__APIKEY: str = ""

    # Processing
    # ----------
    KUBERNETES_INCLUSTER: bool = False
    KUBERNETES_JOB_IMAGE: str = "registry.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform/addons/sobloo/tile-map/runner:latest"
    KUBERNETES_JOB_NAMESPACE: str = "tiles-generator-jobs"
    KUBERNETES_JOB_RESOURCES_CPU: str = "1"
    KUBERNETES_JOB_RESOURCES_MEMORY: str = "4Gi"
    KUBERNETES_JOB_RETRIES: int = 1
    KUBERNETES_JOB_RETRY_INTERVAL: int = 300

    # Max zoom level for which tiles will be computed
    MAX_ZOOM: int = 15
    # Number of concurrent processes for computing tiles
    # TODO: leave this to 1 for now, doesn't work in conjunction with subprocess.call
    NUMBER_OF_PROCESSES: int = 1
    # Maximum cloud probability for which a pixel is considered as non-cloudy
    MAX_CLOUD_PROBABILITY: float = 30.0
    # Directory where images are downloaded and tiles are stored after processing
    WORKING_DIR: Path = PurePath(__file__).parent.parent / "data/dev/working_directory/"


config = Settings()
