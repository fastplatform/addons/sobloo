import logging

from gql import Client
from gql.transport.aiohttp import AIOHTTPTransport, log as aiohttp_logger
from gql.client import AsyncClientSession

from app.settings import config

logger = logging.getLogger(__name__)
aiohttp_logger.setLevel(logging.WARNING)


class GraphQLClient:
    client: Client
    session: AsyncClientSession

    def __init__(self):

        transport = AIOHTTPTransport(
            url=config.CATALOG_URL,
            headers={
                "X-Hasura-Admin-Secret": config.CATALOG_ADMIN_SECRET_KEY,
                "Content-type": "application/json",
            },
        )
        self.client = Client(
            transport=transport,
            fetch_schema_from_transport=True,
            execute_timeout=config.CATALOG_TIMEOUT,
        )

    async def connect(self):
        await self.client.transport.connect()
        self.session = AsyncClientSession(client=self.client)

    async def execute(self, *args, **kwargs):
        return await self.session.execute(*args, **kwargs)

    async def close(self):
        await self.client.transport.close()


graphql_client = GraphQLClient()
