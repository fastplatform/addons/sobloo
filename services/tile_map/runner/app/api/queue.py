import logging

from gql import gql

from app.settings import config
from app.db.graphql_clients import graphql_client
from app.lib.kubernetes import kubernetes_client


logger = logging.getLogger(__name__)


async def queue(args):
    """Create new Kubernetes jobs for all the unprocessed satellite images
    in our database
    """

    # Start clients
    await graphql_client.connect()
    await kubernetes_client.connect_client()

    # Get failed jobs waiting to be retried
    jobs = await kubernetes_client.get_image_processing_jobs()
    failed_jobs = [
        j
        for j in jobs.items
        if j.status.conditions and j.status.conditions[0].type.lower() == "failed"
    ]

    # Query the images that have either the RGB or the NDVI not processed (in the
    # catalogue)
    query = (
        config.APP_DIR / "api/graphql/query_satellite_images_not_processed.graphql"
    ).read_text()
    data = await graphql_client.execute(
        gql(query),
        variable_values={
            "max_attempts": config.KUBERNETES_JOB_RETRIES,
            "max_concurrency": args.max_concurrency + len(failed_jobs),
        },
    )

    logger.info(
        "Creating %s satellite image processing jobs",
        len(data["satellite_image"]) - len(failed_jobs),
    )

    # Submit jobs
    for satellite_image in data["satellite_image"]:
        await kubernetes_client.submit_satellite_image_processing_job(
            satellite_image["id"]
        )

    # Stop clients
    await graphql_client.close()
    await kubernetes_client.close_client()
