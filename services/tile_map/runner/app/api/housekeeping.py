import logging

from gql import gql

from app.db.graphql_clients import graphql_client
from app.lib.kubernetes import kubernetes_client
from app.settings import config


logger = logging.getLogger(__name__)


async def housekeeping(args):
    """Perform housekeeping tasks, ie:
    - delete completed jobs
    - remove from S3 the satellite images that are not in the database anymore
    - check that the satellite image processings that we have in the database also
      are present in S3
    """

    await graphql_client.connect()
    await kubernetes_client.connect_client()

    logger.info("Performing houskeeping tasks ...")

    # clean succeessful processing jobs
    successful_jobs = await kubernetes_client.clean_successful_satellite_image_processing_jobs()
    logger.info(f"{len(successful_jobs)} successful proccessing Kubernetes jobs cleaned")

    # clean failed processing jobs
    failed_jobs = await kubernetes_client.clean_failed_satellite_image_processing_jobs()
    logger.info(f"{len(failed_jobs)} failed proccessing Kubernetes jobs cleaned")

    # increment nb_processing_attempts for all cleaned jobs
    jobs = successful_jobs + failed_jobs
    satellite_image_ids = [ j.metadata.labels["tile-map-runner-process-image-id"] for j in jobs ]
    mutation = (
        config.APP_DIR / "api/graphql/mutation_update_satellite_images_inc_by_one_nb_processing_attempts.graphql"
    ).read_text()
    data = await graphql_client.execute(
        gql(mutation),
        variable_values={"ids": satellite_image_ids},
    )

    await graphql_client.close()
    await kubernetes_client.close_client()

