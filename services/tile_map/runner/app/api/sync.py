from datetime import datetime, timedelta, timezone
import logging
import json
import asyncio

from gql import gql

from app.settings import config
from app.lib.eodag import eodag_client
from app.db.graphql_clients import graphql_client
from app.lib.s3 import s3_client


logger = logging.getLogger(__name__)


async def sync(args):
    """
    Update our internal catalog with the content of the DataHub catalog for our area of
    interest
    """

    # Get the existing products from any of the catalogs
    hits = eodag_client.search()

    # If we found nothing, we can successfully stop here
    if not hits:
        logger.warning('No products found using any of the providers')
        return

    # Get the current products we have in the database
    try:
        await graphql_client.connect()
        logger.info("Querying our own catalog...")
        query = (
            config.APP_DIR / "api/graphql/query_satellite_images.graphql"
        ).read_text()
        response = await graphql_client.execute(gql(query))
        logger.info(
            "Querying our own catalog: found %s results",
            len(response["satellite_image"]),
        )

        satellite_images = {
            satellite_image["id"]: satellite_image
            for satellite_image in response["satellite_image"]
        }

        hits = {
            hit.id: hit
            for hit in hits
        }

        # Compare the 2 above
        for hit_id, hit in hits.items():
            if hit_id in satellite_images:
                # We already have this image, check if we have received new sources and
                # append them to our sources if so
                our_sources_providers = [s["properties"]["eodag_provider"] for s in satellite_images[hit_id]["sources"]]
                new_sources = [ns for ns in hit.sources if ns["properties"]["eodag_provider"] not in our_sources_providers]
                satellite_images[hit_id]["sources"].extend(new_sources)
            else:
                satellite_images[hit_id] = json.loads(hit.json())

        satellite_images_to_upsert = {}
        satellite_images_to_delete = {}
        
        # We will be deleting all images older than RETENTION_PERIOD days
        cutoff_datetime = (
            datetime.now().replace(tzinfo=timezone.utc)
            - timedelta(days=config.RETENTION_PERIOD)
        )

        for satellite_image_id, satellite_image in satellite_images.items():
            if datetime.fromisoformat(satellite_image['acquisition_date']).replace(tzinfo=timezone.utc) < cutoff_datetime:
                satellite_images_to_delete[satellite_image_id] = satellite_image
            elif satellite_image_id in hits:
                satellite_images_to_upsert[satellite_image_id] = satellite_image

        # Create new images
        logging.info("Upserting %s images to our catalog", len(satellite_images_to_upsert))
        objects = list(satellite_images_to_upsert.values())

        mutation = (
            config.APP_DIR / "api/graphql/mutation_insert_satellite_images.graphql"
        ).read_text()

        for idx in range(0, len(objects), config.CATALOG_WRITE_CHUNK_SIZE):
            response = await graphql_client.execute(
                gql(mutation),
                variable_values={
                    "objects": objects[idx : idx + config.CATALOG_WRITE_CHUNK_SIZE]
                },
            )

        # Delete images from our catalog (if they are too old)
        if satellite_images_to_delete:
            satellite_image_ids = [
                satellite_image["id"] for satellite_image in satellite_images_to_delete.values()
            ]
            logging.info(
                "Deleting %s images from our catalog: %s%s",
                len(satellite_image_ids),
                ", ".join(satellite_image_ids[:10]),
                "..." if len(satellite_image_ids) > 10 else "",
            )
            mutation = (
                config.APP_DIR / "api/graphql/mutation_delete_satellite_images.graphql"
            ).read_text()
            for idx in range(
                0, len(satellite_image_ids), config.CATALOG_DELETE_CHUNK_SIZE
            ):
                response = await graphql_client.execute(
                    gql(mutation),
                    variable_values={
                        "satellite_image_ids": satellite_image_ids[
                            idx : idx + config.CATALOG_DELETE_CHUNK_SIZE
                        ]
                    },
                )

            # Delete them from S3 as well
            logging.info(
                "Deleting %s images from S3 as well", len(satellite_images_to_delete)
            )
            for satellite_image in satellite_images_to_delete.values():
                try:
                    logger.info("Deleting %s from S3", satellite_image["id"])
                    s3_client.delete_satellite_image_processings(satellite_image["id"])
                except Exception as e:
                    logger.error('Failed to delete satellite image "%s" from S3: %s', satellite_image["id"], e)

    finally:
        await graphql_client.close()

    logging.info("Done")
