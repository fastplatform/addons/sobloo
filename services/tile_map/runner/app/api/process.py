import os
import logging
import subprocess
import time
import shutil
import multiprocessing

from gql import gql

from app.settings import config
from app.lib.eodag import eodag_client
from app.lib.s3 import s3_client
from app.db.graphql_clients import graphql_client

logger = logging.getLogger(__name__)


async def process(args: str):
    """Run the processing (RGB or NDVI) for a given satellite image"""

    await graphql_client.connect()
    start = time.time()

    try:
        # Get the image name from our database
        # Will of course fail if the image does not exist
        query = (
            config.APP_DIR / "api/graphql/query_satellite_image_sources.graphql"
        ).read_text()
        data = await graphql_client.execute(
            gql(query), variable_values={"satellite_image_id": args.satellite_image_id}
        )
        if not data["satellite_image"]:
            raise Exception(
                f"Satellite image with id={args.satellite_image_id} does not exist",
            )

        satellite_image_sources = data["satellite_image"][0]["sources"]
        satellite_image_nb_processing_attempts = data["satellite_image"][0][
            "nb_processing_attempts"
        ]

        # Make sure our output directories exist and are empty
        if (config.WORKING_DIR / args.satellite_image_id).exists():
            shutil.rmtree(config.WORKING_DIR / args.satellite_image_id)
        os.makedirs(
            config.WORKING_DIR / args.satellite_image_id / "download", exist_ok=True
        )
        os.makedirs(config.WORKING_DIR / args.satellite_image_id / "rgb", exist_ok=True)
        os.makedirs(
            config.WORKING_DIR / args.satellite_image_id / "ndvi", exist_ok=True
        )

        # Try downloading the image using any of the "sources" previously stored
        has_downloaded = eodag_client.download(
            args.satellite_image_id, satellite_image_sources
        )

        # No image was downloaded, fail
        if not has_downloaded:
            if (
                satellite_image_nb_processing_attempts + 1
                >= config.KUBERNETES_JOB_RETRIES
            ):
                logger.error(
                    "Max attempts reached for image %s, processing will not be further retried",
                    args.satellite_image_id,
                )
            raise SystemError(
                "Failed downloading image %s via all means", args.satellite_image_id
            )

        logger.info("Downloaded image %s", args.satellite_image_id)

        def compute_and_upload_rgb(satellite_image_id):
            """Compute and upload the RGB tiles"""
            logger.info("Process RGB tiles for %s", satellite_image_id)
            return_code = subprocess.call(
                [
                    str(config.APP_DIR / "scripts/rgb_processing.sh"),
                    str(config.WORKING_DIR / satellite_image_id / "download"),
                    str(config.WORKING_DIR / satellite_image_id / "rgb"),
                    str(config.MAX_ZOOM),
                    str(config.NUMBER_OF_PROCESSES),
                ], start_new_session=True,
            )
            if return_code != 0:
                raise ChildProcessError(
                    f"RGB processing terminated with codes: {return_code}"
                )

            logger.info("Upload RGB tiles for %s", satellite_image_id)
            s3_client.upload_satellite_image_processing(satellite_image_id, "rgb")

        def compute_and_upload_ndvi(satellite_image_id):
            """Compute and upload the NDVI tiles"""
            logger.info("Process NDVI tiles for %s", satellite_image_id)
            return_code = subprocess.call(
                [
                    str(config.APP_DIR / "scripts/ndvi_processing.sh"),
                    str(config.WORKING_DIR / satellite_image_id / "download"),
                    str(config.WORKING_DIR / satellite_image_id / "ndvi"),
                    str(config.MAX_ZOOM),
                    str(config.NUMBER_OF_PROCESSES),
                    str(config.MAX_CLOUD_PROBABILITY),
                ], start_new_session=True,
            )
            if return_code != 0:
                raise ChildProcessError(
                    f"NDVI processing terminated with codes: {return_code}"
                )

            logger.info("Upload NDVI tiles for %s", satellite_image_id)
            s3_client.upload_satellite_image_processing(satellite_image_id, "ndvi")

        rgb_process = multiprocessing.Process(
            target=compute_and_upload_rgb, args=(args.satellite_image_id,)
        )

        ndvi_process = multiprocessing.Process(
            target=compute_and_upload_ndvi, args=(args.satellite_image_id,)
        )

        # Start the processes
        rgb_process.start()
        ndvi_process.start()

        # Wait for completion of both
        rgb_process.join()
        ndvi_process.join()

        # Update our database to publish the new processings
        base_url = f"{config.S3_URL}/{config.S3_BUCKET_NAME}"
        if config.S3_LOCATION:
            base_url += f"/{config.S3_LOCATION}"

        satellite_image_processings = [
            {
                "id": f"{args.satellite_image_id}|rgb",
                "image": args.satellite_image_id,
                "process": "rgb",
                "url": f"{base_url}/{args.satellite_image_id}/rgb",
                "available": True,
            },
            {
                "id": f"{args.satellite_image_id}|ndvi",
                "image": args.satellite_image_id,
                "process": "ndvi",
                "url": f"{base_url}/{args.satellite_image_id}/ndvi",
                "available": True,
            },
        ]

        mutation = (
            config.APP_DIR
            / "api/graphql/mutation_insert_satellite_image_processings.graphql"
        ).read_text()
        data = await graphql_client.execute(
            gql(mutation), variable_values={"objects": satellite_image_processings}
        )
        logger.info(
            "Inserted %s satellite image processings in the database",
            data["insert_satellite_image_processing"]["affected_rows"],
        )

        # Done!
        logger.info(
            "Done in %s seconds for satellite image %s",
            int(time.time() - start),
            args.satellite_image_id,
        )

        return {"OK": True}

    except Exception as exc:
        raise exc

    finally:
        # Cleanup even if the run failed
        logger.info("Cleanup for satellite image %s", args.satellite_image_id)
        shutil.rmtree(config.WORKING_DIR / args.satellite_image_id, ignore_errors=True)

        # Stop clients
        await graphql_client.close()
