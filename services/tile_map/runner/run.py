import argparse
import asyncio
import logging
import sys

from app.settings import config
from app.api.sync import sync
from app.api.process import process
from app.api.queue import queue
from app.api.housekeeping import housekeeping
from app.tracing import Tracing


# Log
logger = logging.getLogger(__name__)
logging.basicConfig(level=config.LOG_LEVEL.upper())

# Debuging instrumentation
if config.REMOTE_DEBUG:
    import debugpy
    debugpy.listen(5678)
    debugpy.wait_for_client()


async def main():
    Tracing.init()  # TODO Clément
    logger.debug(config)

    try:

        parser = argparse.ArgumentParser("run.py")
        subparsers = parser.add_subparsers()

        parser_sync = subparsers.add_parser("sync", help="synchronise the catalog with the Sentinel 2 + Level 2A catalog")
        parser_sync.set_defaults(function=sync)

        parser_queue = subparsers.add_parser("queue", help="queue new jobs for unprocessed images")
        parser_queue.add_argument("--max-concurrency", type=int, default=1, help="the maximum number of parallel jobs to orchestrate")
        parser_queue.set_defaults(function=queue)

        parser_job = subparsers.add_parser("process", help="process a SOBLOO image")
        parser_job.add_argument("satellite_image_id", type=str, help="the satellite image id to process")
        parser_job.set_defaults(function=process)

        parser_housekeeping = subparsers.add_parser("housekeeping", help="perform housekeeping tasks")
        parser_housekeeping.set_defaults(function=housekeeping)

        args = parser.parse_args(sys.argv[1:])
        await args.function(args)

    except Exception as e:
        logging.exception(e)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
