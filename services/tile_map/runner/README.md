# tile-map/runner: Sentinel 2 - RGB/NDVI tile generator

```tile-map/runner``` is a [Python](https://www.python.org/) service that performs the different tasks related to the processing of Sentinel 2 (L2A) imagery into RGB and NDVI tile pyramids.

## Architecture

The service is entirely built in [Python](https://www.python.org/).

It uses the following other services/resources:
- [tile-map/catalog](../catalog): the FaST satellite imagery catalog, based on a [PostGIS](https://www.postgresql.org/) database (stores list of processed images, as well as the backlog of images to process) and a GraphQL API (based on [Hasura](https://hasura.io) that exposes the current catalogue to the FaST application and the current backlog to the processing service
- an object storage service (managed S3) that stores the RGB and NDVI tiles generated by the processing service

The service itself exposes 4 scripts:
  - `sync` to synchronize our catalogue with the current L2A catalogue for the given area
  - `queue` to orchestrate processing tasks on a [Kubernetes](https://kubernetes.io/) cluster
  - `process` to generate RGB and NDVI tiles for a given Sentinel 2 L2A scene, and upload them to an object storage
  - `housekeeping` to perform cleanup tasks such as failed jobs
- 3 cron jobs that trigger the `sync`, `queue` and `housekeeping` scripts at given intervals

The components are deployed within the FaST infrastructure (Kubernetes cluster and managed S3 service), and interact as follows:

```plantuml
@startuml

rectangle "FaST Kubernetes cluster" as k8s {
    control "cron"
    database postgis [
        <b>PostGIS database</b>
        ---
        + ""satellite_image""
        + ""satellite_image_processing""
    ]
    component hasura #line:black [
        <b>GraphQL API Gateway</b>
        ---
        ""query {""
        ""    satellite_image {...}""
        ""    satellite_image_processing {...}""
        ""}""
    ]
    component "tile-map service" as tilemap #lightgrey {
        component "sync" as sync #line:green;text:green
        component "queue" as queue #line:blue;text:blue
        component "process" as process #line:red;text:red
        component "housekeeping" as housekeeping #line:orange;text:orange
    }
}

cloud "Object storage (S3)" as s3 {
    database bucket [
        ""{satellite_image_id}""
        ""    /{rgb/ndvi}""
        ""        /{z}/{x}/{y}.png""

    ]
}

cloud "Sentinel 2 imagery repositories" as s2 {
    collections "Sobloo"
    collections "Onda"
    collections "CreoDIAS"
    collections "SciHub (ESA)"
}

rectangle "FaST app" as app
actor "User" as user

user --> app

app -[hidden]-> k8s

hasura --> postgis #line:black

cron --> sync #green;text:green : Every 30\nminutes
cron --> queue #blue;text:blue : Every 30\nminutes
cron --> housekeeping #orange;text:orange : Every 30\nminutes

sync <-- s2 #green;text:green : (1) Read Sentinel-2\nL2A catalogue
sync --> postgis #green;text:green : (2) Update catalogue
sync -[hidden]left-> queue

queue <-- hasura #blue;text:blue : (1) Get remaining images\nto process
queue --> process #blue;text:blue : (2) Start 5\nconcurrent jobs

process <-- s2 #red;text:red : (1) Download Sentinel 2\nimagery"
process --> bucket #red;text:red : (2) Upload RGB and\nNDVI tiles
process --> postgis #red;text:red : (3) Update\nprocessings

app --> hasura #black;text:black : (1) Retrieve catalogue\nitems for current\nmap viewport
app --> bucket #black;text:black : (2) Retrieve\nRGB/NDVI tiles

@enduml
```

## Environment variables

Below are the available environment variables for the tile-map service, depending on the command being run:
### All commands
- `LOG_LEVEL`: logging level (defaults to `"info"`)
- `CATALOG_URL`: URL of the GraphQL API
- `CATALOG_ADMIN_SECRET_KEY`: Admin key of the GraphQL API
- `CATALOG_TIMEOUT`: Timeout in seconds of connections to the GraphQL API (defaults to `60 * 5`)
- `CATALOG_WRITE_CHUNK_SIZE`: Maximum number of items to write at once in the API (defaults to `500`)
- `CATALOG_DELETE_CHUNK_SIZE`: Maximum number of items to write at once in the API (defaults to `500`)

### `process` command

Image processing parameters:
- `MAX_ZOOM`: Max zoom level for which tiles will be computed (defaults to `15`)
- `NUMBER_OF_PROCESSES`: Number of concurrent processes for computing tiles (defaults to `1`)
- `MAX_CLOUD_PROBABILITY`: Maximum cloud probability for which a pixel is considered as non-cloudy (defaults to `30.0`)
- `WORKING_DIR`: Directory where images are downloaded and tiles are stored after processing

Object storage upload parameters:
- `S3_URL`: S3 service URL (defaults to `"https://oss.eu-west-0.prod-cloud-ocb.orange-business.com"`)
- `S3_BUCKET_NAME`: S3 bucket name
- `S3_ACCESS_KEY_ID` and `S3_SECRET_KEY`: S3 credentials with write access to the bucket
- `S3_LOCATION`: subdirectory (key prefix) where the tiles will be uploaded (defaults to `""`)
- `S3_RETRIES`: Number of retries for uploading a file (defaults to `10`)
- `S3_UPLOAD_CONCURRENCY`: Number of concurrent uploads (defaults to `50`)

### `sync` command
- `AREA_OF_INTEREST`: WKT of the area of interest to watch (defaults to `"POLYGON EMPTY"`)
- `MAX_CLOUD_COVER`: Max cloud cover percentage of images that will be processed (defaults to `30.0`)
- `RETENTION_PERIOD`: Number of days the images/tiles are to be kept in the database/storage (defaults to `365`)
- `SYNCHRONIZATION_PERIOD`: Number of days of images that should be requested from the DIAS catalogue and compared to our own (defaults to `30`)
- `DELETE_LOCAL_IMAGES_NOT_IN_CATALOG`: Whether images we have in our catalog but that do not exist in the Sentinel 2 L2A archive should be deleted (defaults to `False`)

EODAG parameters:
- `EODAG_PROVIDERS`: Providers to activate in EODAG for searching and downloading images (defaults to `"creodias,sobloo,onda"`)
- `EODAG__SCIHUB__API__CREDENTIALS__USERNAME` and `EODAG__SCIHUB__API__CREDENTIALS__PASSWORD`: SciHub credentials
- `EODAG__CREODIAS__AUTH__CREDENTIALS__USERNAME` and `EODAG__CREODIAS__AUTH__CREDENTIALS__PASSWORD`: CREODIAS credentials
- `EODAG__ONDA__AUTH__CREDENTIALS__USERNAME` and `EODAG__ONDA__AUTH__CREDENTIALS__PASSWORD`: ONDA credentials
- `EODAG__SOBLOO__AUTH__CREDENTIALS__APIKEY`: Sobloo credentials

### `queue` command

- `KUBERNETES_INCLUSTER`: (defaults to `False`)
- `KUBERNETES_JOB_IMAGE`: Docker image to be used to run the `process` job (defautls to `"registry.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform/addons/sobloo/tile-map/runner:latest"`)
- `KUBERNETES_JOB_NAMESPACE`: Namespace in which the job will be created (defaults to `"tiles-generator-jobs"`)
- `KUBERNETES_JOB_RESOURCES_CPU`: CPU allocation for the job (defaults to `"1"`)
- `KUBERNETES_JOB_RESOURCES_MEMORY`: Memory allocation for the job (defaults to `"4Gi"`)
- `KUBERNETES_JOB_RETRIES`: Number of retries if the job fails (defaults to `1`)
- `KUBERNETES_JOB_RETRY_INTERVAL`: Interval between retries (defaults to `300`)

## Development 

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the Postgres server and the catalog:
```bash
cd ../catalog
make start-postgres
make start
```

The GraphQL catalog is now available at `http://localhost:8433/graphql`

Run any of the commands:
```bash
make run ARGS="sync"
make run ARGS="process S2A_MSIL2A_20220109T094401_N0301_R036_T34SCJ_20220109T121235"
make run ARGS="queue"
```
