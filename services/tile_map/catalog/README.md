# tile-map/catalog

```tile-map/catalog``` is a [Hasura](https://hasura.io/) service that exposes a single [GraphQL](https://graphql.org/) endpoint, connected to a [Postgres](https://www.postgresql.org/) database which stores a metadata catalog of processed Sentinel images (RGB + NDVI). 

The GraphQL ontology exposed by the service is mounted as a remote schema into the main GraphQL API gateway and is therefore not exposed directly to the public.


- [metadata](metadata) directory contains the Hasura metadata, that define the GraphQL schema
- [migrations](migrations) directory contains the Hasura migrations for the Postgres database

### Environment variables

As the service starts a vanilla Hasura container, its environment variables are the ones supported by the Hasura engine. Most notable variables are:

- `HASURA_ADMIN_SECRET_KEY`:  Hasura admin secret key
- `HASURA_GRAPHQL_DATABASE_URL`: Postgres database URL
- `HASURA_GRAPHQL_ENABLE_CONSOLE`: Enable the Hasura Console
- `HASURA_GRAPHQL_PG_CONNECTIONS`: Maximum number of Postgres connections that can be opened per stripe
- `HASURA_GRAPHQL_PG_TIMEOUT`: Each connection’s idle time before it is closed
- `HASURA_GRAPHQL_PG_CONN_LIFETIME`: Time from connection creation after which the connection should be destroyed and a new one created

## GraphQL / database schema

The database and GraphQL schemas contain only 2 tables/object types:
- satellite_image: represents a Sentinel-2 L2A scene
- satellite_image_processing: represents a processed RGB or NDVI image for a given scene

The exposed GraphQL schema is as follows:

```graphql
query {
    satellite_image (...) {
        idv                       # "S2A_MSIL2A_20220109T094401_N0301_R036_T34SCJ_20220109T121235"
        name                      # "S2A_MSIL2A_20220109T094401_N0301_R036_T34SCJ_20220109T121235"
        mission                   # "Sentinel-2"
        mission_code              # "S2A" or "S2B"
        geometry                  # image footprint in GeoJSON
        cloud_cover               # percentage of clouds
        acquisition_date          # acquisition date
        nb_processing_attempts    # number of processing attempts
        sources                   # source information to download the image
    }
    satellite_image_processing (...) {
        id
        satellite_image {...}     # relationship to satellite_image node
        process                   # "rgb" or "ndvi"
        url                       # base URL of the XYZ tile pyramid
        available                 # true if the tile pyramid is complete
    }
}

```

As the schema is built and served by an Hasura gateway, each query node implements the parameters implemented by the Hasura engine: this includes the geographical operators proposed by PostGIS. For example, the following query returns the current processed images in the catalogue for the current map viewport:

```graphql
# query
query ($bounds: geometry!) {
    satellite_image_processing(
        where: {
            _and: [
                { satellite_image: { geometry: { _st_intersects: $bounds } } }
                { process: { _in: ["ndvi", "rgb"] }
                ]
            }
        order_by: { satellite_image: { acquisition_date: desc } }
        limit: 200
    ) {
        process
        url
        satellite_image {
            cloud_cover
            name
            acquisition_date
        }
    }
}
```

```json
// variables
{
    "bounds": "{\"type\":\"Polygon\",\"coordinates\":[[[24.474057,45.863259],[24.474057,45.872224],[24.504956,45.872224],[24.504956,45.863259],[24.474057,45.863259]]],\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"urn:ogc:def:crs:EPSG::4326\"}}}"
}
```

## Development setup

Have a look at the [Makefile](./Makefile) to see the available development commands.
### Prerequisites

- [Docker](https://www.docker.com/)

### Start the API Gateway

```bash
make start-postgres # Start a local Postgres database
make start
```

The catalog service is now running on: http://localhost:8433 and the Postgres database is running on port 8432.
