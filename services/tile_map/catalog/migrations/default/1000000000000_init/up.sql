CREATE TABLE public.satellite_image (
    id character varying NOT NULL,
    name character varying,
    mission character varying,
    mission_code character varying,
    geometry public.geometry(Polygon,4326),
    cloud_cover numeric,
    acquisition_date date,
    nb_processing_attempts integer DEFAULT 0 NOT NULL
);
CREATE TABLE public.satellite_image_processing (
    id character varying NOT NULL,
    image character varying,
    process character varying,
    url character varying,
    available boolean
);
ALTER TABLE ONLY public.satellite_image
    ADD CONSTRAINT satellite_image_name_key UNIQUE (name);
ALTER TABLE ONLY public.satellite_image
    ADD CONSTRAINT satellite_image_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.satellite_image_processing
    ADD CONSTRAINT satellite_image_processing_image_process_unique UNIQUE (image, process);
ALTER TABLE ONLY public.satellite_image_processing
    ADD CONSTRAINT satellite_image_processing_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.satellite_image_processing
    ADD CONSTRAINT satellite_image_processing_image_fkey FOREIGN KEY (image) REFERENCES public.satellite_image(name) ON DELETE CASCADE;
