#!/bin/bash


set -o errexit
set -o nounset
set -o pipefail


echo $@ | xargs -n 2 diff
