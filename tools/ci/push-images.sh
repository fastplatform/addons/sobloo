#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/common.sh

BAZEL_OPTIONS=
if [ ! -z "${CI}" ]; then
BAZEL_OPTIONS="$BAZEL_OPTIONS -c dbg"
fi

echo $(bazel query 'filter(image_bundle_push, //...)') | xargs -n 1 bazel run $BAZEL_OPTIONS
