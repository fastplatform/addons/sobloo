load("@io_bazel_rules_docker//container:container.bzl", "container_bundle")
load("@io_bazel_rules_docker//contrib:push-all.bzl", "container_push")

def image_bundle(name, images, format, visibility=None):
    """Instantiate rules to manage a bundle of images.
    
       Instantiated rules:
        - :name to build images
        - :name_push to push images
        - :name_manifest to generate a text MANIFEST listing all images with tags
    """
    container_bundle(
        name = name,
        images = images,
        visibility = [
            "//visibility:private",
        ],
    )

    container_push(
        name = name + "_push",
        bundle = name,
        format = format,
        visibility = [
            "//visibility:private",
        ],
    )

    native.genrule(
        name = name + "_manifest",
        srcs = [
            name + ".tar",
        ],
        outs = [
            name + ".tar.MANIFEST",
        ],
        cmd = " && ".join([
            "tar xf $< repositories",
            "$(location //tools:jq)  -j '. as $$g | (keys[] | ., ($$g[.] | \" \", keys[], \"\n\"))' repositories >$@",
        ]),
        tools = [
            "//tools:jq",
        ],
        visibility = visibility,
    )
