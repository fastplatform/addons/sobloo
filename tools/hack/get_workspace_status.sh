#!/bin/bash

git_rev=$(git rev-parse HEAD 2>/dev/null)
echo "BUILD_SCM_REVISION ${git_rev}"

git diff-index --quiet HEAD -- 2>/dev/null
if [[ $? == 0 ]];
then
    tree_status="Clean"
else
    tree_status="Modified"
fi
echo "BUILD_SCM_STATUS ${tree_status}"

GITSHA=$(git describe --always 2>/dev/null)

SOBLOO_CI_VERSION=${SOBLOO_CI_VERSION:=$(grep 'SOBLOO_CI_VERSION\s*=' VERSION | awk '{print $3}')}

if [[ -z "${VERSION}" ]]; then
  if [ "$(git describe --tags --exact-match ${git_rev} >/dev/null 2>&1; echo $?)" -eq 0 ]; then
    VERSION=$(git describe --tags)
  else
    VERSION="${SOBLOO_CI_VERSION}+${GITSHA}"
  fi
fi

echo "STABLE_SOBLOO_VERSION ${VERSION}"

SOBLOO_TAG=${VERSION/+/-}
if [ "$(git describe --tags --exact-match ${git_rev} >/dev/null 2>&1; echo $?)" -eq 0 ]; then
  SOBLOO_TAG=${SOBLOO_TAG}-release-${GITSHA}
  SOBLOO_TAG=${SOBLOO_TAG/+/-}
fi
echo "STABLE_SOBLOO_TAG ${SOBLOO_TAG}"
echo "STABLE_SOBLOO_TAG_PREFIXED_WITH_COLON :${SOBLOO_TAG}"

if [[ -z "${DOCKER_REGISTRY}" ]]; then
  DOCKER_REGISTRY="registry.gitlab.com"
fi
if [[ -z "${DOCKER_IMAGE_PREFIX}" ]]; then
  DOCKER_IMAGE_PREFIX=fastplatform/addons/sobloo/
fi
echo "STABLE_DOCKER_REGISTRY ${DOCKER_REGISTRY}"
echo "STABLE_DOCKER_IMAGE_PREFIX ${DOCKER_IMAGE_PREFIX}"
