load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@distroless//package_manager:dpkg.bzl", "dpkg_list", "dpkg_src")
load("@io_bazel_rules_docker//container:container.bzl", "container_pull")
load("@io_bazel_rules_docker//python3:image.bzl", _py_image_repos = "repositories")
load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")
load("@rules_python_external//:defs.bzl", "pip_install")

def transitive_deps():
    _py_image_repos()

def docker_deps():
    container_deps()
    maybe(
        container_pull,
        name = "py3.7_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:025b77e95e701917434c478e7fd267f3d894db5ca74e5b2362fe37ebe63bbeb0",
    )
    maybe(
        container_pull,
        name = "py3.7_debug_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:ebd73d8f4da293c9826e8646137a05260ecd1b7ee103cb1f62ebf010fda7c7f9",
    )
    maybe(
        container_pull,
        name = "hasura_graphql_engine_image_base",
        registry = "index.docker.io",
        repository = "hasura/graphql-engine",
        digest = "sha256:3db179db8cfeb2d6add0d6a726ca5feee6bcbbdafd8798270c87889f3f7aa8b9",
    )

def dpkg_deps():
    dpkg_src(
        name = "debian10_snap",
        arch = "amd64",
        distro = "buster",
        sha256 = "f251129edc5e5b31dadd7bb252e5ce88b3fdbd76de672bc0bbcda4f667d5f47f",
        snapshot = "20200612T083553Z",
        url = "https://snapshot.debian.org/archive",
    )
    dpkg_src(
        name = "debian10_updates_snap",
        arch = "amd64",
        distro = "buster-updates",
        sha256 = "24b35fcd184d71f83c3f553a72e6636954552331adfbbc694f0f70bd33e1a2b4",
        snapshot = "20200612T083553Z",
        url = "https://snapshot.debian.org/archive",
    )
    dpkg_src(
        name = "debian10_security_snap",
        package_prefix = "https://snapshot.debian.org/archive/debian-security/20200612T105246Z/",
        packages_gz_url = "https://snapshot.debian.org/archive/debian-security/20200612T105246Z/dists/buster/updates/main/binary-amd64/Packages.gz",
        sha256 = "c0ae35609f2d445e73ca8d3c03dc843f5ddae50f474cee10e79c4c1284ce2a2d",
    )
    dpkg_list(
        name = "packages_debian10",
        packages = [
            "coreutils",
            "curl",
            "debconf",
            "dpkg",
            "fontconfig-config",
            "fonts-dejavu-core",
            "fonts-liberation",
            "gcc-8-base",
            "gdal-bin",
            "gdal-data",
            "install-info",
            "libacl1",
            "libaec0",
            "libarmadillo9",
            "libarpack2",
            "libattr1",
            "libblas3",
            "libbz2-1.0",
            "libc6",
            "libcharls2",
            "libcom-err2",
            "libcurl3-gnutls",
            "libcurl4",
            "libdap25",
            "libdapclient6v5",
            "libdapserver7v5",
            "libdb5.3",
            "libepsilon1",
            "libexpat1",
            "libffi6",
            "libfontconfig1",
            "libfreetype6",
            "libfreexl1",
            "libfyba0",
            "libgcc1",
            "libgcrypt20",
            "libgdal20",
            "libgeos-3.7.1",
            "libgeos-c1v5",
            "libgeotiff2",
            "libgfortran5",
            "libgif7",
            "libgmp10",
            "libgnutls30",
            "libgpg-error0",
            "libgssapi-krb5-2",
            "libhdf4-0-alt",
            "libhdf5-103",
            "libhogweed4",
            "libicu63",
            "libidn2-0",
            "libjbig0",
            "libjpeg62-turbo",
            "libjson-c3",
            "libk5crypto3",
            "libkeyutils1",
            "libkmlbase1",
            "libkmlconvenience1",
            "libkmldom1",
            "libkmlengine1",
            "libkmlregionator1",
            "libkmlxsd1",
            "libkrb5-3",
            "libkrb5support0",
            "liblapack3",
            "liblcms2-2",
            "libldap-2.4-2",
            "libldap-common",
            "libltdl7",
            "liblzma5",
            "libmariadb3",
            "libminizip1",
            "libmpdec2",
            "libncursesw6",
            "libnetcdf13",
            "libnettle6",
            "libnghttp2-14",
            "libnspr4",
            "libnss3",
            "libodbc1",
            "libogdi3.2",
            "libopenjp2-7",
            "libp11-kit0",
            "libpcre3",
            "libpng16-16",
            "libpoppler82",
            "libpopt0",
            "libpq5",
            "libproj13",
            "libpsl5",
            "libqhull7",
            "libquadmath0",
            "libreadline7",
            "librtmp1",
            "libsasl2-2",
            "libsasl2-modules-db",
            "libselinux1",
            "libspatialite7",
            "libsqlite3-0",
            "libssh2-1",
            "libssl1.1",
            "libstdc++6",
            "libsuperlu5",
            "libsz2",
            "libtasn1-6",
            "libtiff5",
            "libtinfo6",
            "libunistring-dev",
            "libunistring2",
            "liburiparser1",
            "libuuid1",
            "libwebp6",
            "libxerces-c3.2",
            "libxml2",
            "libzstd1",
            "mariadb-common",
            "mime-support",
            "mysql-common",
            "odbcinst",
            "odbcinst1debian2",
            "perl-base",
            "proj-data",
            "python3-gdal",
            "python3-numpy",
            "python3-pkg-resources",
            "readline-common",
            "sensible-utils",
            "tar",
            "ttf-bitstream-vera",
            "ucf",
            "zlib1g",
        ],
        # Takes the first package found: security updates should go first
        sources = [
            "@debian10_security_snap//file:Packages.json",
            "@debian10_updates_snap//file:Packages.json",
            "@debian10_snap//file:Packages.json",
        ],
    )

def pip_deps():
    maybe(
        pip_install,
        name = "tile_map_runner_pip",
        requirements = "//services/tile_map/runner:requirements.txt",
    )
    maybe(
        pip_install,
        name = "tile_map_runner_pip_dev",
        requirements = "//services/tile_map/runner:requirements-dev.txt",
    )    

def deps_step_1():
    transitive_deps()
    docker_deps()
    dpkg_deps()
    pip_deps()
