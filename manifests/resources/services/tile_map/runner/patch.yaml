apiVersion: builtin
kind: PatchTransformer
metadata:
  name: tile-map-runner-housekeeping
patch: |-
  - op: add
    path: /metadata/labels
    value:
      deploy.fastplatform.eu/db-requirement: initialized
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/command'
    value:
      - /bin/sh
      - -c
      - |
        until curl -fsI http://localhost:15020/healthz/ready; do echo \"Waiting for sidecar proxy ...\"; python -c 'import time;time.sleep(1)'; done;
        python ../../*.binary housekeeping
        exit_code=$(echo $?);
        curl -fsI -X POST http://localhost:15020/quitquitquit && exit $exit_code
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/imagePullPolicy'
    value: Always
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/env'
    value:
      - name: CATALOG_ADMIN_SECRET_KEY
        valueFrom:
          secretKeyRef:
            key: catalog-admin-key
            name: tile-map-runner-secret
      - name: CATALOG_TIMEOUT
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.catalog-timeout)
      - name: CATALOG_URL
        value: http://tile-map-catalog.$(fastplatform.global.namespace)/v1/graphql
      - name: KUBERNETES_INCLUSTER
        value: "true"
      - name: KUBERNETES_JOB_IMAGE
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.kubernetes-job-image)
      - name: KUBERNETES_JOB_NAMESPACE
        value: $(fastplatform.global.namespace)
      - name: KUBERNETES_JOB_RESOURCES_CPU
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-job-resources-cpu)
      - name: KUBERNETES_JOB_RESOURCES_MEMORY
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-job-resources-memory)
      - name: KUBERNETES_JOB_RETRY_INTERVAL
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-job-retry-interval)
      - name: LOG_LEVEL
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.log-level)
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/resources'
    value:
      limits:
        cpu: "$(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.housekeeping.resources.cpu)"
        memory: "$(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.housekeeping.resources.memory)"
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/restartPolicy'
    value: Never
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/serviceAccountName'
    value: tile-map-runner-housekeeping
  - op: add
    path: '/spec/jobTemplate/spec/backoffLimit'
    value: 1
  - op: add
    path: '/spec/schedule'
    value: $(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.housekeeping.schedule)
  - op: add
    path: '/spec/concurrencyPolicy'
    value: Forbid
  - op: add
    path: '/spec/failedJobsHistoryLimit'
    value: 1
  - op: add
    path: '/spec/successfulJobsHistoryLimit'
    value: 0
target:
  group: batch
  kind: CronJob
  name: tile-map-runner-housekeeping
  version: v1beta1
---
apiVersion: builtin
kind: PatchTransformer
metadata:
  name: tile-map-runner-queue
patch: |-
  - op: add
    path: /metadata/labels
    value:
      deploy.fastplatform.eu/db-requirement: initialized
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/command'
    value:
      - /bin/sh
      - -c
      - |
        until curl -fsI http://localhost:15020/healthz/ready; do echo \"Waiting for sidecar proxy ...\"; python -c 'import time;time.sleep(1)'; done;
        python ../../*.binary queue --max-concurrency=$(fastplatform.addons.sobloo.services.tile-map.runner.processing-job-paralelism);
        exit_code=$(echo $?);
        curl -fsI -X POST http://localhost:15020/quitquitquit && exit $exit_code
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/imagePullPolicy'
    value: Always
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/env'
    value:
      - name: CATALOG_ADMIN_SECRET_KEY
        valueFrom:
          secretKeyRef:
            key: catalog-admin-key
            name: tile-map-runner-secret
      - name: CATALOG_TIMEOUT
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.catalog-timeout)
      - name: CATALOG_URL
        value: http://tile-map-catalog.$(fastplatform.global.namespace)/v1/graphql
      - name: KUBERNETES_INCLUSTER
        value: "true"
      - name: KUBERNETES_JOB_IMAGE
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.kubernetes-job-image)
      - name: KUBERNETES_JOB_NAMESPACE
        value: $(fastplatform.global.namespace)
      - name: KUBERNETES_JOB_RESOURCES_CPU
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-job-resources-cpu)
      - name: KUBERNETES_JOB_RESOURCES_MEMORY
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-job-resources-memory)
      - name: KUBERNETES_JOB_RETRIES
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-job-retries)
      - name: LOG_LEVEL
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.log-level)
      - name: AREA_OF_INTEREST
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.sync-area-of-interest.wkt)
      - name: MAX_CLOUD_COVER
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.sync-max-cloud-cover)
      - name: DELETE_LOCAL_IMAGES_NOT_IN_CATALOG
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.sync-delete-local-images-not-in-catalog)
      - name: MAX_CLOUD_PROBABILITY
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-max-cloud-probability)
      - name: MAX_ZOOM
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-max-zoom)
      - name: NUMBER_OF_PROCESSES
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.processing-number-of-processes)
      - name: S3_ACCESS_KEY_ID
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-access-key)
      - name: S3_BUCKET_NAME
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-bucket-name)
      - name: S3_LOCATION
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-location)
      - name: S3_URL
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-url)
      - name: S3_RETRIES
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-retries)
      - name: S3_SECRET_KEY
        valueFrom:
          secretKeyRef:
            key: s3-secret-key
            name: tile-map-runner-secret
      - name: S3_UPLOAD_CONCURRENCY
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-upload-concurrency)
      - name: EODAG_PROVIDERS
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.eodag-providers)
      - name: EODAG__SOBLOO__AUTH__CREDENTIALS__APIKEY
        valueFrom:
          secretKeyRef:
            key: sobloo-apikey
            name: tile-map-runner-secret
      - name: EODAG__SCIHUB__API__CREDENTIALS__USERNAME
        valueFrom:
          secretKeyRef:
            key: scihub-username
            name: tile-map-runner-secret
      - name: EODAG__SCIHUB__API__CREDENTIALS__PASSWORD
        valueFrom:
          secretKeyRef:
            key: scihub-password
            name: tile-map-runner-secret
      - name: EODAG__CREODIAS__AUTH__CREDENTIALS__USERNAME
        valueFrom:
          secretKeyRef:
            key: creodias-username
            name: tile-map-runner-secret
      - name: EODAG__CREODIAS__AUTH__CREDENTIALS__PASSWORD
        valueFrom:
          secretKeyRef:
            key: creodias-password
            name: tile-map-runner-secret
      - name: EODAG__ONDA__AUTH__CREDENTIALS__USERNAME
        valueFrom:
          secretKeyRef:
            key: onda-username
            name: tile-map-runner-secret
      - name: EODAG__ONDA__AUTH__CREDENTIALS__PASSWORD
        valueFrom:
          secretKeyRef:
            key: onda-password
            name: tile-map-runner-secret
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/resources'
    value:
      limits:
        cpu: "$(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.queue.resources.cpu)"
        memory: "$(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.queue.resources.memory)"
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/restartPolicy'
    value: Never
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/serviceAccountName'
    value: tile-map-runner-queue
  - op: add
    path: '/spec/jobTemplate/spec/backoffLimit'
    value: 1
  - op: add
    path: '/spec/schedule'
    value: $(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.queue.schedule)
  - op: add
    path: '/spec/concurrencyPolicy'
    value: Forbid
  - op: add
    path: '/spec/failedJobsHistoryLimit'
    value: 1
  - op: add
    path: '/spec/successfulJobsHistoryLimit'
    value: 0
target:
  group: batch
  kind: CronJob
  name: tile-map-runner-queue
  version: v1beta1
---
apiVersion: builtin
kind: PatchTransformer
metadata:
  name: tile-map-runner-sync
patch: |-
  - op: add
    path: /metadata/labels
    value:
      deploy.fastplatform.eu/db-requirement: initialized
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/command'
    value:
      - /bin/sh
      - -c
      - |
        until curl -fsI http://localhost:15020/healthz/ready; do echo \"Waiting for sidecar proxy ...\"; python -c 'import time;time.sleep(1)'; done;
        python ../../*.binary sync;
        exit_code=$(echo $?);
        curl -fsI -X POST http://localhost:15020/quitquitquit && exit $exit_code
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/imagePullPolicy'
    value: Always
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/env'
    value:
      - name: AREA_OF_INTEREST
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.sync-area-of-interest.wkt)
      - name: CATALOG_ADMIN_SECRET_KEY
        valueFrom:
          secretKeyRef:
            key: catalog-admin-key
            name: tile-map-runner-secret
      - name: CATALOG_TIMEOUT
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.catalog-timeout)
      - name: CATALOG_URL
        value: http://tile-map-catalog.$(fastplatform.global.namespace)/v1/graphql
      - name: LOG_LEVEL
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.log-level)
      - name: MAX_CLOUD_COVER
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.sync-max-cloud-cover)
      - name: RETENTION_PERIOD
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.sync-retention-period)
      - name: SYNCHRONIZATION_PERIOD
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.sync-synchronization-period)
      - name: S3_ACCESS_KEY_ID
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-access-key)
      - name: S3_BUCKET_NAME
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-bucket-name)
      - name: S3_LOCATION
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-location)
      - name: S3_URL
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-url)
      - name: S3_RETRIES
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-retries)
      - name: S3_SECRET_KEY
        valueFrom:
          secretKeyRef:
            key: s3-secret-key
            name: tile-map-runner-secret
      - name: S3_UPLOAD_CONCURRENCY
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.s3-upload-concurrency)
      - name: DELETE_LOCAL_IMAGES_NOT_IN_CATALOG
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.sync-delete-local-images-not-in-catalog)
      - name: EODAG_PROVIDERS
        value: $(fastplatform.addons.sobloo.services.tile-map.runner.eodag-providers)
      - name: EODAG__SOBLOO__AUTH__CREDENTIALS__APIKEY
        valueFrom:
          secretKeyRef:
            key: sobloo-apikey
            name: tile-map-runner-secret
      - name: EODAG__SCIHUB__API__CREDENTIALS__USERNAME
        valueFrom:
          secretKeyRef:
            key: scihub-username
            name: tile-map-runner-secret
      - name: EODAG__SCIHUB__API__CREDENTIALS__PASSWORD
        valueFrom:
          secretKeyRef:
            key: scihub-password
            name: tile-map-runner-secret
      - name: EODAG__CREODIAS__AUTH__CREDENTIALS__USERNAME
        valueFrom:
          secretKeyRef:
            key: creodias-username
            name: tile-map-runner-secret
      - name: EODAG__CREODIAS__AUTH__CREDENTIALS__PASSWORD
        valueFrom:
          secretKeyRef:
            key: creodias-password
            name: tile-map-runner-secret
      - name: EODAG__ONDA__AUTH__CREDENTIALS__USERNAME
        valueFrom:
          secretKeyRef:
            key: onda-username
            name: tile-map-runner-secret
      - name: EODAG__ONDA__AUTH__CREDENTIALS__PASSWORD
        valueFrom:
          secretKeyRef:
            key: onda-password
            name: tile-map-runner-secret
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/containers/0/resources'
    value:
      limits:
        cpu: "$(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.sync.resources.cpu)"
        memory: "$(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.sync.resources.memory)"
  - op: add
    path: '/spec/jobTemplate/spec/template/spec/restartPolicy'
    value: Never
  - op: add
    path: '/spec/jobTemplate/spec/backoffLimit'
    value: 1
  - op: add
    path: '/spec/schedule'
    value: $(fastplatform.addons.sobloo.services.tile-map.runner.cronjob.sync.schedule)
  - op: add
    path: '/spec/concurrencyPolicy'
    value: Forbid
  - op: add
    path: '/spec/failedJobsHistoryLimit'
    value: 1
  - op: add
    path: '/spec/successfulJobsHistoryLimit'
    value: 0
target:
  group: batch
  kind: CronJob
  name: tile-map-runner-sync
  version: v1beta1
