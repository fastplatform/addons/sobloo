# Sobloo: earth imagery service add-on

Additional module that implements [OGC TMS (Tile Map Service)](https://en.wikipedia.org/wiki/Tile_Map_Service) for [sobloo](https://sobloo.eu/) imagery.

This repository includes the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

## Services

### Tile Map

- [tile-map/catalog](services/tile_map/catalog)
- [tile-map/runner](services/tile_map/runner)
