# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

## Bazel targets

.PHONY: build
build: ## Build services
	bazel build //...

.PHONY: test
test: ## Test services
	bazel test //...

.PHONY: scan-secrets
scan-secrets: ## Scan for secrets
	./tools/ci/scan-secrets.sh

.PHONY: audit-secrets
audit-secrets: ## Audit secret scan findings (cf .secrets.baseline)
	./tools/hack/ensure-detect-secrets.sh
	detect-secrets audit .secrets-baseline

.PHONY: update-bazel-config
update-bazel-config: ## Update Bazel configurations
	./tools/hack/update-bazel.sh

.PHONY: run-ci
run-ci: scan-secrets build test ## Run CI pipeline locally
	@echo "[+] CI pipeline completed successfully, you are ready to push !"

.PHONY: clean
clean: ## Clean all
	bazel clean --expunge
